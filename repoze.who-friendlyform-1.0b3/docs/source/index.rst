*****************************************************
Collection of :mod:`repoze.who` friendly form plugins
*****************************************************

:Author: Gustavo Narea.
:Latest release: |release|

.. module:: repoze.who.plugins.friendlyform
    :synopsis: Developer-friendly repoze.who form plugins
.. moduleauthor:: Gustavo Narea <me@gustavonarea.net>

.. topic:: Overview

    **repoze.who-friendlyform** is a :mod:`repoze.who` plugin which provides
    a collection of developer-friendly form plugins, although for the time
    being such a collection has only one item.


How to install
==============

The minimum requirement is :mod:`repoze.who`, and you can install both with
``easy_install``::
    
    easy_install repoze.who-friendlyform


Available form plugins
======================

.. autoclass:: FriendlyFormPlugin
    :members: __init__


Support and development
=======================

The prefered place to ask questions is the `Repoze mailing list 
<http://lists.repoze.org/listinfo/repoze-dev>`_ or the `#repoze 
<irc://irc.freenode.net/#repoze>`_ IRC channel. Bugs reports and feature 
requests should be sent to `the issue tracker of the Repoze project 
<http://bugs.repoze.org/>`_.

The development mainline is available at the following Subversion repository::

    http://svn.repoze.org/whoplugins/whofriendlyforms/trunk/


Releases
--------

.. toctree::
    :maxdepth: 2
    
    News
